package goif1

import "testing"

func TestHello(t *testing.T) {
    want := "Hellound so"
    if got := Hello(); got != want {
        t.Errorf("Hello() = %q, want %q", got, want)
    }
}
